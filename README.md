# Town Issue Review Board

## Project Description

Town Issue Review Board is a service that allows a town to communicate issues and meetings between its constituents and council members.  This is done by allowing constituents to submit issues.  The town council members then can view the issues and schedule meetings.  These meetings then can be seen by the town members, with important details such as location and date displaye..

Below is a link to the overview of what the Town Issue Review Board software does.

!(https://gitlab.com/AdamRanieri/node-gcp-batch/-/blob/main/Projects/Project2.md)

## Technologies Used

* GCP Kubernetes Engine
* Docker
* Terraform
* NodeJS
* Cloud SQL
* Cloud Pub/Sub
* Cloud Run
* Cloud Datastore

## Features

List of features ready
* An issue ingestion service that allows constituents to submit their issues through pub/sub
* A REST API where only council members can submit meetings to Cloud SQL
* An authorization service that requires a password that is passed to the REST API to only allow council members to change meetings
* A frontend built in React and Hosted on Firebase using material ui
* A Datastore database that stores all submitted issues


## Service Repositories

### Issue Ingetion
https://gitlab.com/project592/project-2-issue-ingestion

### Town Meeting Scheduler
https://gitlab.com/project592/project-2-town-meeting-scheduler

### Frontend
https://gitlab.com/project592/project-2-front-end

### Issue Analysis Service
https://gitlab.com/project592/project-2-issue-analysis-service

## Project Participants
- Willis Larson (https://gitlab.com/willislarsonm)
- Jesse Esquivel (https://gitlab.com/j.esqui.g)
- Michael Eyo (https://gitlab.com/meyo101)
- Aldrin Caalim (https://gitlab.com/aldrincaalim)
