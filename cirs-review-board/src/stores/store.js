import { createStore } from "redux";

const reducer = (loginState  = {authToken:""}, message) =>{
    if(message.type === "login"){
        const newLoginState = loginState
        newLoginState.authToken = message.authToken;// update state with new username
        return newLoginState;
    }
    if(message.type === "logout"){
        const newLoginState = loginState
        newLoginState.authToken = "";// update state with new username
        return newLoginState;
    }
    return loginState;
}

const store = createStore(reducer); // we have created out store

export default store;