import * as React from 'react';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import { useDispatch } from "react-redux"

export default function FormDialog(props) {
  const [open, setOpen] = React.useState(false);
  const dispatch = useDispatch()

  const handleClickOpen = () => {
    setOpen(true);
  };
  
  const handleClose = () => {
    setOpen(false);
  };
  
  let loginValue = "";
  
  
  const login = () =>{
    const authenticationObject = {type:"login", authToken: loginValue}
    dispatch(authenticationObject);
    console.log(authenticationObject);
    props.setLocal(loginValue);
    console.log(loginValue);
    handleClose();
  }
  
  function changeLoginValue(event) {
    loginValue = event.target.value;
  } 

  return (
    <div>
      <Button variant="outlined" onClick={handleClickOpen}>
        Council Login
      </Button>
      <Dialog open={open} onClose={handleClose}>
        <DialogTitle>Authenticate</DialogTitle>
        <DialogContent>
          <DialogContentText>
            To Access Council Member Options, Please Input Secret Council Member Token
          </DialogContentText>
          <TextField
            autoFocus
            margin="dense"
            id="name"
            label="Token"
            type="email"
            fullWidth
            variant="standard"
            onChange={changeLoginValue}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Cancel</Button>
          <Button onClick={login}>Submit</Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}