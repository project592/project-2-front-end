import * as React from "react";
import MeetingsTable from "./meetings-table";
import axios from "axios";
import Button from '@mui/material/Button';
import "./styles/view-meetings.css";

export default function ViewMeetings() {
    const [meetings, setMeetings] = React.useState();
    const urlScheduledMeetings = `http://35.222.37.218/meetings`;

    async function getAllMeetings() {
        const response = await axios.get(`${urlScheduledMeetings}`);
        console.log(response.data);
        setMeetings(response.data);
    }
    
    return (
        <div>
            <h1>Meetings</h1>
            <div className="anonymous-view-meetings">
                <div className="button">
                    <Button variant="contained" onClick={getAllMeetings}>Get All Scheduled Meetings</Button>
                </div>
            </div>
                <div className="anonymous-meetings-table">
                    {meetings === undefined ? <div></div> : <MeetingsTable meeting={meetings}></MeetingsTable>}
                </div>
        </div>
    )

}
