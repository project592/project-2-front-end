import * as React from 'react';
import IssuesTable from './issues-table';
import Last24HoursTable from "./last-24-hours-table";
import Last7DaysTable from "./last-7-days-table";
import Button from '@mui/material/Button';
import axios from "axios";
import "./styles/review-issues.css";

export default function ReviewIssues() {

    const urlIssueAnalysisPod = `http://34.122.4.78`;
    const urlReports = `${urlIssueAnalysisPod}/issues/report`;
    const issueType = React.useRef(null);
    const reviewType = React.useRef(null);
    const highlightType = React.useRef(null);
    const datePosted = React.useRef(null);
    const dateOfIssue = React.useRef(null);
    const [issues, setIssues] = React.useState();
    const [last24Hours, setLast24Hours] = React.useState();
    const [last7Days, setLast7Days] = React.useState();

    const dummyData = [
        {
            datePosting: "today",
            dateIssue: "february",
            issueDescription: "february description",
            location: "1st Street",
            issueType: "other",
            reviewed: false,
            highlighted: false,
        },
        {
            datePosting: "yesterday",
            dateIssue: "march",
            issueDescription: "march description",
            location: "3rd Street",
            issueType: "pollution",
            reviewed: false,
            highlighted: false,
        },
        {
            datePosting: "tomorrow",
            dateIssue: "april",
            issueDescription: "april description",
            location: "2nd Street",
            issueType: "noise/disturbing the peace",
            reviewed: true,
            highlighted: true,
        },
        {
            datePosting: "next week",
            dateIssue: "may",
            issueDescription: "may description",
            location: "1st Street",
            issueType: "public health",
            reviewed: false,
            highlighted: false,
        }
];
    
    async function getAllIssues() {
        const grabIssues = await axios.get(`${urlIssueAnalysisPod}/issues`);
        const showIssues = grabIssues.data;
        console.log(grabIssues.data);
        // const response = dummyData;
        setIssues(showIssues);
    };

    
    async function getIssueOfCertainType() {
        const grabIssues = await axios.get(`${urlIssueAnalysisPod}/issues`);
        const showIssues = grabIssues.data;
        // console.log("sfirst", showIssues);
        const theIssueType = issueType.current.value;
        const specificIssueType = showIssues.filter(function(data) {
            return data.issueType === theIssueType;
        });
        // console.log("filter", specificIssueType);
        setIssues(specificIssueType);
    }

    async function getIssueByDatePosted() {
        const dPosted = datePosted.current.value;
        const grabIssues = await axios.get(`${urlIssueAnalysisPod}/issues`);
        const showIssues = grabIssues.data;
        console.log(dPosted);
        console.log("sfirst", showIssues[0].datePosting);

        const sameDate = showIssues.filter(function(date) {
            return date.datePosting === dPosted;
        });
        console.log("sameDate", sameDate);
        setIssues(sameDate);
        // const theIssueType = issueType.current.value;
        // const specificIssueType = showIssues.filter(function(data) {
        //     return data.issueType === theIssueType;
        // });
        // // console.log("filter", specificIssueType);
        // setIssues(specificIssueType);
    }
    async function getIssueByDateOfIssue() {
        const dIssued = dateOfIssue.current.value;
        const grabIssues = await axios.get(`${urlIssueAnalysisPod}/issues`);
        const showIssues = grabIssues.data;
        console.log(dIssued);
        console.log("sfirst", showIssues[0].datePosting);

        const sameDate = showIssues.filter(function(date) {
            return date.dateIssue === dIssued;
        });
        console.log("sameDate", sameDate);
        setIssues(sameDate);
        // const theIssueType = issueType.current.value;
        // const specificIssueType = showIssues.filter(function(data) {
        //     return data.issueType === theIssueType;
        // });
        // // console.log("filter", specificIssueType);
        // setIssues(specificIssueType);
    }
    
    async function getReviewedIssues() {
        const grabIssues = await axios.get(`${urlIssueAnalysisPod}/issues`);
        const showIssues = grabIssues.data;
        const theReviewType = reviewType.current.value;
        const unreviewed = showIssues.filter(function(item) {
            // console.log("unreviewed filter", item.reviewed === false);
            // console.log(Boolean(item.reviewed));
            return item.reviewed === "false";
        });
        const reviewed = showIssues.filter(function(item) {
            return item.reviewed === "true";
        });
        // console.log("unreviewed", unreviewed);
        // console.log("reviewed", reviewed);
        // console.log("thousand", showIssues[56].reviewed);
        // console.log("hudnred", dummyData);
        // const unreviewed = dummyData.filter(function(data) {
            //     return data.reviewed === false;
        // });
        // const reviewed = dummyData.filter(function(data) {
        //     return data.reviewed === true;
        // });
        // console.log("length", showIssues.length);
        for (let i = 0; i < showIssues.length; i++) {
            if (theReviewType === "reviewed") {
                console.log("setIssues reviewed:", reviewed);
                setIssues(reviewed);
            } else if (theReviewType === "unreviewed") {
                console.log("setIssues unreviewed:", unreviewed);
                setIssues(unreviewed);
            }
        }
    }
    
    async function getHighlightedIssues() {
        const grabIssues = await axios.get(`${urlIssueAnalysisPod}/issues`);
        const showIssues = grabIssues.data;
        const theHighlightType = highlightType.current.value;
        const highlight = showIssues.filter(function(data) {
            return data.highlight === "true";
        });
        const unhighlight = showIssues.filter(function(data) {
            return data.highlight === "false";
        });

        for (let i = 0; i < showIssues.length; i++) {
            if (theHighlightType === "highlight") {
                setIssues(highlight);
            } else if (theHighlightType === "unhighlight") {
                // console.log(dummyData[i]);
                setIssues(unhighlight);
            }
        }
    }

    // async function getInfo() {
    //     const response = await axios.get(urlReports);
    //     console.log(response.data);
    // }

    async function getLast24Hours() {
        const response = await axios.get(urlReports);
        const issues = response.data.last24Hours;
        console.log(response.data.last24Hours);
        const arr = Object.entries(issues);
        const object = {
            infrastructure: arr[0][1],
            noise: arr[1][1],
            pollution: arr[2][1],
            publicHealth: arr[3][1],
            safety: arr[4][1]
        };
        // const screwy = arr.forEach(function(item) {
            //     return item[1];
            // })
            // console.log(screwy);
        setLast24Hours([object]);
        }
        
    async function getLast7Days() {
        const response = await axios.get(urlReports);
        const issues = response.data.last7Days;
        const arr = Object.entries(issues);
        const object = {
            infrastructure: arr[0][1],
            noise: arr[1][1],
            pollution: arr[2][1],
            publicHealth: arr[3][1],
            safety: arr[4][1]
        };
        console.log(response.data.last7Days);
        setLast7Days([object]);
    }

    return (
        <div>
            <h1>Review Issues</h1>
            <div className="button-get-all-issues">
                <Button onClick={getAllIssues} variant="contained">Get All Issues</Button>
            </div>

            <div className="big-section">
                <div className="issue-type-section">
                    <select id="issueType" name="issueType" ref={issueType}>
                        <option value="">Select an Issue Type</option>
                        <option value="infrastructure">Infrastructure</option>
                        <option value="safety">Safety</option>
                        <option value="public health">Public Health</option>
                        <option value="pollution">Pollution</option>
                        <option value="noise/disturbing the peace">Noise/Disturbing the peace</option>
                        <option value="other">Other</option>
                    </select>
                    <Button onClick={getIssueOfCertainType} variant="contained">Issue Type</Button>
                </div>

                <div className="review-type-section">
                    <select id="reviewType" name="reviewType" ref={reviewType}>
                        <option value="">Select a Review Type</option>
                        <option value="reviewed">Reviewed</option>
                        <option value="unreviewed">Unreviewed</option>
                    </select>
                    <Button onClick={getReviewedIssues} variant="contained">Reviewed/Unreviewed Issues</Button>
                </div>

                <div className="highlight-type-section">
                    <select id="highlightType" name="highlightType" ref={highlightType}>
                        <option value="">Select Highlight Type</option>
                        <option value="highlight">Highlighted</option>
                        <option value="unhighlight">Unhighlighted</option>
                    </select>
                    <Button onClick={getHighlightedIssues} variant="contained">Highlighted/Not Highlighted</Button>
                </div>

                <div className="query-date-posted-section">
                    <input ref={datePosted}></input>
                    <Button onClick={getIssueByDatePosted} variant="contained">Get by Date Posted</Button>
                </div>
                <div className="query-date-issued-section">
                    <input ref={dateOfIssue}></input>
                    <Button onClick={getIssueByDateOfIssue} variant="contained">Get by Issued Date</Button>
                </div>
                <div className="button-last-24">
                    <Button onClick={getLast24Hours} variant="contained">Issues sent last 24 hours</Button>
                </div>
                <div className="button-last-7">
                <Button onClick={getLast7Days} variant="contained">Issues sent last 7 days</Button>
                </div>
            </div>
            <div>
                <h2>Last 24 Hours</h2>
                 {last24Hours === undefined ? <div></div> : <Last24HoursTable last24={last24Hours}></Last24HoursTable>}
            </div>
            <div>
                <h2>Last 7 Days</h2>
            {last7Days === undefined ? <div></div> : <Last7DaysTable last7={last7Days}></Last7DaysTable>}
            </div>
            <div>
                <h2>Issues</h2>
            {issues === undefined ? <div></div> : <IssuesTable issue={issues}></IssuesTable>}
            </div>
            
        </div>
    )
}
