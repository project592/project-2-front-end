export default function MeetingsTable(props) {
    const last7DaysIssues = props.last7 || [];

    const tableRows = last7DaysIssues.map((last) => (
        <tr>
            <td>{last.infrastructure}</td>
            <td>{last.noise}</td>
            <td>{last.pollution}</td>
            <td>{last.publicHealth}</td>
            <td>{last.safety}</td>
            <td>{last.other}</td>
        </tr>
    ));

    return (
        <table>
            <thead>
            <th>Infrastructure</th>
                <th>Noise/Disturbing the Peace</th>
                <th>Pollution</th>
                <th>Public Health</th>
                <th>Safety</th>
                <th>Other</th>
            </thead>
            <tbody>{tableRows}</tbody>
        </table>
    )
}
