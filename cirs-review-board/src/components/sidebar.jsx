import * as React from 'react';
import Box from '@mui/material/Box';
import Drawer from '@mui/material/Drawer';
import Button from '@mui/material/Button';
import List from '@mui/material/List';
import Divider from '@mui/material/Divider';
import ListItem from '@mui/material/ListItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import InboxIcon from '@mui/icons-material/MoveToInbox';
import MailIcon from '@mui/icons-material/Mail';
import EventIcon from '@mui/icons-material/Event';
import CommentIcon from '@mui/icons-material/Comment';
import CancelIcon from '@mui/icons-material/Cancel';
import FormDialog from '../components/loginmodal';
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";

export default function TemporaryDrawer() {

  const loginState = {authToken:useSelector((state) => state.authToken)};
  
    const [localLogin, setLocal] = React.useState(loginState.authToken);

  const [state, setState] = React.useState({
    top: false,
    left: false,
    bottom: false,
    right: false,
  });

  const toggleDrawer = (anchor, open) => (event) => {
    if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
      return;
    }

    setState({ ...state, [anchor]: open });
  };

  const list = (anchor) => (
    <Box
      sx={{ width: anchor === 'top' || anchor === 'bottom' ? 'auto' : 250 }}
      role="presentation"
      //onClick={toggleDrawer(anchor, false)}
      //onKeyDown={toggleDrawer(anchor, false)}
    >
      <List>
        
          <ListItem button key={'Exit Menu'} onClick={toggleDrawer(anchor, false)}>
            <ListItemIcon>
              <CancelIcon/>
            </ListItemIcon>
            <ListItemText primary={'Exit Menu'} />
          </ListItem>
        
      </List>
      <Divider />
      <List>
         <Link to={"/submit"}>
          <ListItem button key={'Submit Issue'} onClick={toggleDrawer(anchor, false)}>
            <ListItemIcon>
              <CommentIcon/>
            </ListItemIcon>
            <ListItemText primary={'Submit Issue'} />
          </ListItem>
          </Link>
          
          <Link to={"/view"}>
          <ListItem button key={'View Meetings'} onClick={toggleDrawer(anchor, false)}>
            <ListItemIcon>
              <EventIcon/>
            </ListItemIcon>
            <ListItemText primary={'View Meetings'} />
          </ListItem>
          </Link>

        <ListItem>
            <FormDialog setLocal={setLocal}/>
        </ListItem>
      </List>
      {localLogin && <>
      <Divider />
      <List>
        
        <Link to={"/review"}>
          <ListItem button key={'Review Issues'} onClick={toggleDrawer(anchor, false)}>
            <ListItemIcon>
              <InboxIcon />
            </ListItemIcon>
            <ListItemText primary={'Review Issues'} />
          </ListItem>
        </Link>

        <Link to={"/schedule"}>
          <ListItem button key={'Schedule Meetings'} onClick={toggleDrawer(anchor, false)}>
          <ListItemIcon>
            <MailIcon />
          </ListItemIcon>
          <ListItemText primary={'Schedule Meetings'} />
        </ListItem>
        </Link>

      </List></>
      }
    </Box>
  );

  function checkState() {
    console.log(loginState.authToken);
  }

  return (
    <div>
      {['Menu'].map((anchor) => (
        <React.Fragment key={anchor}>
          <Button onClick={toggleDrawer(anchor, true)}>{anchor}</Button>
          <Drawer
            anchor={anchor}
            open={state[anchor]}
            onClose={toggleDrawer(anchor, false)}
          >
            {list(anchor)}
          </Drawer>
        </React.Fragment>
      ))}
      {/* <button onClick={checkState}>Check State</button> */}
    </div>
  );
}
