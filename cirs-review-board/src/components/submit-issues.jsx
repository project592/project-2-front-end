import { FormControl, InputLabel, MenuItem, Select, input } from '@mui/material';
import * as React from 'react';
import Button from '@mui/material/Button';
import axios from "axios";
import TextField from '@mui/material/TextField';
import "./styles/submit-issues.css";
export default function SubmitIssues() {

    const dateIssue = React.useRef(null)
    const issueDescription = React.useRef(null);
    const location = React.useRef(null);
    const issueType = React.useRef(null);

    async function submitIssue(event) {
        const dIssue = dateIssue.current.value.toLowerCase();
        const iDescription = issueDescription.current.value.toLowerCase();
        const loc = location.current.value.toLowerCase();
        const iType = issueType.current.value.toLowerCase();

        const body = {
            dateIssue: dIssue,
            issueDescription: iDescription,
            location: loc,
            issueType: iType
        };

        console.log(body);
        const result = await axios.post(`https://town-issue-injestion-y2uvhc7eya-uc.a.run.app/submitissue`, body);
        console.log(result);
    }

    return(
        <div>
            <div className="container">
            <h1>Post Your Issue Here</h1>
            <label name="dateIssue">Date Issue</label>
            <input id="dateIssue" type="date" placeholder="Date of Issue" ref={dateIssue}></input>

            <label name="issueDescription">Issue Description</label>
            <input placeholder="issueDescription" ref={issueDescription}></input>

            <label name="location">Location</label>
            <input placeholder="location" ref={location}></input>
            <InputLabel id="issueType">Issue Type</InputLabel>
            <select id="issueType" name="issueType" ref={issueType}>
                <option value="">Select an Option</option>
                <option value="infrastructure">Infrastructure</option>
                <option value="safety">Safety</option>
                <option value="public health">Public Health</option>
                <option value="pollution">Pollution</option>
                <option value="noise/disturbing the peace">Noise/Disturbing the peace</option>
                <option value="other">Other</option>
            </select>

            <Button onClick={submitIssue} variant="contained">Submit</Button>
            </div>
        </div>
    )
}
