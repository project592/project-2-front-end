import axios from "axios";
import Button from '@mui/material/Button';
// import "./styles/review-issues.css";

export default function IssuesTable(props) {
    const issuesArray = props.issue || [];
    const urlIssueAnalysisPod = `http://34.122.4.78`;

    const tableRows = issuesArray.map((i) => (
        <tr>
            <td>{i.datePosting}</td>
            <td>{i.dateIssue}</td>
            <td>{i.issueDescription}</td>
            <td>{i.location}</td>
            <td>{i.issueType}</td>
            <td>{i.reviewed === "false" ? "Not Reviewed by Council" : "Reviewed by Council"}</td>
            <td>{i.highlight === "false" ? "Not Highlighted" : "Highlighted"}</td>
            <td><Button variant="contained" onClick={() => {
                makeReview(i.id)
            }}>{i.reviewed === "false" ? "Reviewed" : "Unreviewed"}</Button></td>
            <td><Button  variant="contained" onClick={() => {
                makeHighlight(i.id)
            }}>{i.highlight === "false" ? "Highlighted" : "Unhighlighted"}</Button></td>
        </tr>
    ));

    async function makeReview(id) {
        await axios.get(`${urlIssueAnalysisPod}/review/${id}`);
    }

    async function makeHighlight(id) {
        await axios.get(`${urlIssueAnalysisPod}/highlight/${id}`);
    }

    return (
        <table>
            <thead>
                <th>Date Posting</th>
                <th>Date Issue</th>
                <th>Issue Description</th>
                <th>Location</th>
                <th>Issue Type</th>
                <th>Reviewed</th>
                <th>Highlighted</th>
                <th>Reviewed/Unreviewed</th>
                <th>Highlight/Unhighlight</th>
            </thead>
            <div className="tableBody">
            <tbody>{tableRows}</tbody>
            </div>
        </table>
    )
}