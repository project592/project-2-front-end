import * as React from 'react';
import MeetingsTable from './meetings-table';
import axios from "axios";
import Button from '@mui/material/Button';
import { useSelector } from "react-redux";
import "./styles/schedule-meetings.css";

export default function ScheduleMeetings() {

    const loginState = {authToken:useSelector((state) => state.authToken)};

    const location = React.useRef(null);
    const date = React.useRef(null);
    const topicDiscussion = React.useRef(null);
    const [meetings, setMeetings] = React.useState();

    const urlScheduledMeetings = `http://35.222.37.218/meetings`;

    async function createMeeting() {
        // const loginState = {authToken:useSelector((state) => state.authToken)};
        const theLocation = location.current.value;
        const theDate = date.current.value;
        const theTopicDiscussion = topicDiscussion.current.value;

        const meetingsObject = {
            meeting_location: theLocation,
            meeting_time: theDate,
            meeting_topic: theTopicDiscussion
        };

        const response = await axios.post(`${urlScheduledMeetings}`, meetingsObject,{
            headers: {
                authorization: loginState.authToken
            }
        });

        const singleArray = [response.data];
        console.log(singleArray);
        setMeetings(singleArray);
    }

    async function getAllMeetings() {
        const response = await axios.get(`${urlScheduledMeetings}`);
        console.log(response.data);
        setMeetings(response.data);
    }

    async function testButton() {
        const testURL = "http://35.222.37.218/meetings";
        const meetingsObject = {
            meeting_location: 'Castle',
            meeting_time: '2022-12-12',
            meeting_topic: 'Empty'
        };
        const response = await axios.get(testURL);
        const res = await axios.get(`${testURL}/meetings`, {
            headers: {
                authorization: loginState.authToken
            } 
        });
        // const res = await axios.post(`${testURL}`,
        //  meetingsObject, {
        //     headers: {
        //         authorization: loginState.authToken
        //     }
        // });
        console.log(res);
        res.send(res);
    }

    function checkTheState() {
        console.log(loginState.authToken);
    }

    return (
        <div>
            <h1>Schedule Meetings</h1>
            <div className="schedule-meetings-input">
            <label name="location">Location</label>
            <input placeholder="Location" ref={location}></input>
            <label name="date">Date</label>
            <input id="date" type="date" placeholder="Schedule Date" ref={date}></input>
            <label name="topicDiscussion">Topic Discussion</label>
            <input placeholder="Topics of Discussion" ref={topicDiscussion}></input>
            <Button onClick={createMeeting} variant="contained">Create Meeting</Button>
                <Button onClick={getAllMeetings} variant="contained">Get All Meetings</Button>
            </div>
            <div>
                {/* <Button onClick={saysHi}variant="contained">Contained</Button> */}

            </div>
            {/* <button onClick={testButton}>Testing</button> */}
            {/* <button onClick={checkTheState}>Check the Auth State</button> */}
            <div className="schedule-meetings-table">
                {meetings === undefined ? <div></div> : <MeetingsTable meeting={meetings}></MeetingsTable>}
            </div>
        </div>
    )
}
