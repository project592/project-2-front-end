export default function MeetingsTable(props) {
    const meetingsArray = props.meeting || [];

    const tableRows = meetingsArray.map((m) => (
        <tr>
            <td>{m.meeting_location}</td>
            <td>{m.meeting_time}</td>
            <td>{m.meeting_topic}</td>
        </tr>
    ));

    return (
        <table>
            <thead>
                <th>Location</th>
                <th>Date</th>
                <th>Topic of Discussion</th>
            </thead>
            <tbody>{tableRows}</tbody>
        </table>
    )
}
