import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import store from './stores/store'
import TemporaryDrawer from './components/sidebar'
import { BrowserRouter as Router, Route } from "react-router-dom";
import SubmitIssues from './components/submit-issues';
import ReviewIssues from './components/review-issues';
import ScheduleMeetings from './components/schedule-meetings';
import ViewMeetings from './components/view-meetings';
import "./components/styles/styles.css";

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <Router>
        <TemporaryDrawer/>
        <Route path="/submit">
          <SubmitIssues></SubmitIssues>
        </Route>
        <Route path="/view">
          <ViewMeetings></ViewMeetings>
        </Route>
        <Route path="/review">
          <ReviewIssues></ReviewIssues>
        </Route>
        <Route path="/schedule">
          <ScheduleMeetings></ScheduleMeetings>
        </Route>
      </Router>
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);
